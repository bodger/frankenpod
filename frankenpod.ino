#include <FatReader.h>
#include <SdReader.h>
#include <avr/pgmspace.h>
#include "WaveUtil.h"
#include "WaveHC.h"

void lsR(FatReader & d);
void playit(FatReader & dir);
void printName(dir_t & dir);

SdReader  card;   // information for the card
FatVolume vol;    // information for the partition on the card
FatReader root;   // information for the filesystem on the card
FatReader f;      // information for the file we're playing
WaveHC    wave;   // This is the only wave (audio) object,
                  // since we will only play one at a time

// here is where we define the buttons that we'll use.
byte      buttons[] = {14, 15, 16, 17};
byte      leds[]    = { 6,  7,  8,  9};

// This handy macro lets us determine how big the array up above is
#define NUMBUTTONS  sizeof(buttons)
#define NUMLEDS     sizeof(buttons)

// we will track if a button is just pressed, just released, or 'pressed'

volatile byte pressed[NUMBUTTONS];
volatile byte justpressed[NUMBUTTONS];
volatile byte justreleased[NUMBUTTONS];

uint8_t dirLevel;   // indent level for file/dir names    (for prettyprinting)
uint8_t nwaves;     // number of wave files available
uint8_t whichwave;
dir_t   dirBuf;     // buffer for directory reads

// filenames for the buttons (if any)

char  file1[13];
char  file2[13];
char  file3[13];
char  file4[13];
char  filename[13];

// this handy function will return the number of bytes currently free in RAM,
// great for debugging!   
int
freeRam(void)
{
  extern int    __bss_end; 
  extern int *  __brkval; 
  int           free_memory; 

  if ((int) __brkval == 0) {
    return(((int) &free_memory) - ((int) &__bss_end));
  }

  return(((int) &free_memory) - ((int) __brkval));
} 

void
sdErrorCheck(void)
{
  if (!card.errorCode()) {
    return;
  }

  putstring("\n\rSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  putstring(", ");
  Serial.println(card.errorData(), HEX);

  for (;;) {
    // loop forever
  }
}

void
setup()
{
  byte    pin;
  uint8_t part;
  
  // set up serial port
  Serial.begin(9600);
  putstring_nl("FrankenPod with ");
  Serial.print(NUMBUTTONS, DEC);
  putstring_nl("buttons and ");
  Serial.print(NUMLEDS, DEC);
  putstring_nl("LEDs");
  
  // This can help with debugging, running out of RAM is bad
  putstring("Free RAM: ");

  // if this is under 150 bytes it may spell trouble!
  Serial.println(freeRam());
  
  // Set the output pins for the DAC control.
  // These pins are defined in the library
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);

  // set LED pins to output

  for (pin=0; pin < NUMLEDS; ++pin) {
    pinMode(leds[pin], OUTPUT);
  }
 
  // pin 13 LED
  pinMode(13, OUTPUT);
 
  // Make input & enable pull-up resistors on switch pins
  for (pin=0; pin < NUMBUTTONS; ++pin) {
    pinMode(buttons[pin], INPUT);
    digitalWrite(buttons[pin], HIGH);
  }
  
  //play with 4 MHz spi if 8MHz isn't working for you
 //   if (!card.init(true)) {

  //play with 8 MHz spi (default faster!)  
  if (!card.init()) {
    // Something went wrong, lets print out why
    punt("Card init. failed");
  }
  
  // enable optimized read - some cards may timeout.
  // Disable if you're having problems
  card.partialBlockRead(true);
 
  // look for a FAT partition - we have up to 5 slots to look in

  for (part = 0; part < 5; ++part) {
    if (vol.init(card, part))  {
      // we found one, lets bail
      break;                   
    }
  }

  if (part == 5) {
    // we ended up not finding one :(
    punt("No valid FAT partition!");
  }
  
  // Let's tell the user about what we found
  putstring("Using partition ");
  Serial.print(part, DEC);
  putstring(", type is FAT");
  Serial.println(vol.fatType(), DEC);     // FAT16 or FAT32?
  
  // Try to open the root directory
  if (!root.openRoot(vol)) {
    punt("Can't open root dir!"); // Something went wrong,
  }
  
  // Whew! We got past the tough parts.
  putstring_nl("Files on card:");
  
  dirLevel = 0;
  nwaves   = 0;

  // Print out all of the files in all the directories, count WAV files
  lsR(root);
  
  Serial.print(nwaves, DEC);
  putstring_nl(" wave files found");

  // set up timer

  TCCR2A = 0;
  TCCR2B = 1<<CS22 | 1<<CS21 | 1<<CS20;

  //Timer2 Overflow Interrupt Enable
  TIMSK2 |= 1<<TOIE2;

  // initialize random number generator

  randomSeed(analogRead(0));
}

SIGNAL(TIMER2_OVF_vect) {
  check_switches();
}

void
check_switches()
{
  static byte previousstate[NUMBUTTONS];
  static byte currentstate[NUMBUTTONS];
  byte        index;

  for (index = 0; index < NUMBUTTONS; ++index) {
    currentstate[index] = digitalRead(buttons[index]);   // read the button
        
    if (currentstate[index] == previousstate[index]) {
      if ((pressed[index] == LOW) && (currentstate[index] == LOW)) {
          // just pressed
          justpressed[index] = 1;
      }
      else if ((pressed[index] == HIGH) && (currentstate[index] == HIGH)) {
          // just released
          justreleased[index] = 1;
      }
      pressed[index] = !currentstate[index];  // remember, digital HIGH means NOT pressed
    }
    previousstate[index] = currentstate[index];   // keep a running tally of the buttons
  }
}

void
loop()
{
  byte i;
  
  if (justpressed[0]) {
    justpressed[0] = 0;

    if (*file1 != 0) {
      playfile(file1);
    } else {
      playrandom();
    }
  }

  if (justpressed[1]) {
    justpressed[1] = 0;

    if (*file2 != 0) {
      playfile(file2);
    } else {
      playrandom();
    }
  }

  if (justpressed[2]) {
    justpressed[2] = 0;

    if (*file3 != 0) {
      playfile(file3);
    } else {
      playrandom();
    }
  }

  if (justpressed[3]) {
    justpressed[3] = 0;

    if (*file4 != 0) {
      playfile(file4);
    } else {
      playrandom();
    }
  }
  
//  putstring_nl("playing random selection...");
//  playrandom();
}

void
playrandom(void)
{
  root.rewind();
  
  whichwave = random(nwaves);
  
  putstring("choosing number ");
  Serial.println(whichwave, DEC);
  
  playit(root);
}

void
playit(
  FatReader & dir)
{
  FatReader   file;

  // Read every file in the directory one at a time

  while (dir.readDir(dirBuf) > 0) {
    // skip . and .. directories
    if (dirBuf.name[0] == '.')  {
      continue;
    }
        
    if (!file.open(vol, dirBuf)) {       // open the file in the directory
      punt("file.open failed");  // something went wrong :(
    }
    
    if (file.isDir()) {                    // check if we opened a new directory
      if (whichwave == 0) {
        return;
      }
    } else {
      // Aha! we found a file that isnt a directory
      if (iswav() &&
          ((dirBuf.name[0] < '1') ||
           (dirBuf.name[0] > '4'))) {
        if (whichwave == 0) {
          copyfilename(filename, dirBuf.name);
          putstring("playing ");
          Serial.println(filename);
          playfile(filename);

          return;
        }

        --whichwave;
      }
    }
  }
}

void
playfile(
  char * name)
{
  FatReader  file;
  
  // see if the wave object is currently doing something

  if (wave.isplaying) {
    // already playing something, so stop it!
    wave.stop();
  }

  // look in the root directory and open the file
  if (!file.open(root, name)) {
    putstring("Couldn't open file ");
    Serial.print(name);
    return;
  }

  // OK read the file and turn it into a wave object
  if (!wave.create(file)) {
    putstring_nl("Not a valid WAV");
    return;
  }
    TIMSK2 &= ~(1<<TOIE2);

  // ok time to play! start playback
  wave.play();
  
  while (wave.isplaying) {
    doleds();
    delay(100);
  }

  // everything OK?

  sdErrorCheck();
  
  ledsoff();

  TIMSK2 |= 1<<TOIE2;
}

// print dir_t name field.
// The output is 8.3 format, so like SOUND.WAV or FILENAME.DAT
void
printName(
  dir_t & dir)
{
  uint8_t pos;

  // 8.3 format has 8+3 = 11 letters in it

  for (pos = 0; pos < 11; ++pos) {
    if (dir.name[pos] == ' ') {
      // dont print any spaces in the name
      continue;        
    }

    if (pos == 8)  {
        Serial.print('.');           // after the 8th letter, place a dot
    }

    Serial.print(dir.name[pos]);      // print the n'th digit
  }

  if (DIR_IS_SUBDIR(dir)) {
    // directories get a / at the end
    Serial.print('/');    
  }
}

void
copyfilename(
  char *     dst,
  uint8_t *  src)
{
  uint8_t    pos;

   for (pos = 0; pos < 11; ++pos) {
     if (*src != ' ') {
       *dst++ = *src++;
     } else {
       ++src;
     }
     if (pos == 7) {
       *dst++ = '.';
     }
   }
   
   *dst = 0;
}

// list recursively - possible stack overflow if subdirectories too nested

void
lsR(
  FatReader & d)
{
  int8_t      r;                     // indicates the level of recursion
  uint8_t     space;
  uint8_t		used;
  
  used = 0;
  
  // read the next file in the directory 
  while ((r = d.readDir(dirBuf)) > 0) {
    // skip subdirs . and ..
    if (dirBuf.name[0] == '.') {
      continue;
    }
      
    switch (dirBuf.name[0]) {
      case '.': // skip any hidden files
        continue;
        
      case '1':  // file for button 1
        copyfilename(file1, dirBuf.name);
        Serial.print("button 1: ");
        Serial.println(file1);
        used = 1;
        break;
        
      case '2':  // file for button 2
        copyfilename(file2, dirBuf.name);
        Serial.print("button 2: ");
        Serial.println(file2);
        used = 1;
        break;
        
      case '3':  // file for button 3
        copyfilename(file3, dirBuf.name);
        Serial.print("button 3: ");
        Serial.println(file3);
        used = 1;
        break;
        
      case '4':  // file for button 4
        copyfilename(file4, dirBuf.name);
        Serial.print("button 4: ");
        Serial.println(file4);
        used = 1;
        break;
    }
    
    for (space = 0; space < dirLevel; ++space) {
      // this is for prettyprinting, put spaces in front
      Serial.print(' ');
    }

    printName(dirBuf);          // print the name of the file we just found
    Serial.println();           // and a new line
    
    if (DIR_IS_SUBDIR(dirBuf)) {   // we will recurse on any direcory
      FatReader s;                 // make a new directory object to hold information
      dirLevel += 2;               // indent 2 spaces for future prints

      if (s.open(vol, dirBuf)) {
        lsR(s);                    // list all the files in this directory now!
      }

      dirLevel -=2;                // remove the extra indentation
    } else {
      if (iswav() &&
          ((dirBuf.name[0] < '1') ||
           (dirBuf.name[0] > '4'))) {
            Serial.println("using for random");
        ++nwaves;
      }
    }
  }

  sdErrorCheck();                  // are we doing OK?
}

int
iswav(void)
{
  return((dirBuf.name[ 8] == 'W') &&
         (dirBuf.name[ 9] == 'A') &&
         (dirBuf.name[10] == 'V'));
}

void
punt(
  char *  msg)
{
  Serial.println(msg);

  sdErrorCheck();

  // then 'halt' - do nothing!

  for (;;) {
  }
}

void
doleds()
{
  setleds(random(16));
}

void
ledsoff()
{
  setleds(0);
}

void
setleds(
  uint8_t  which)
{
  uint8_t  pin;

  for (pin = 0; pin < NUMLEDS; ++pin) {
    digitalWrite(leds[pin], (which & 0x01) ? HIGH : LOW);

    which >>= 1;
  }
}

