# frankenpod

Made for a friend, I replaced the guts of a toy music player with an Arduino, Wave Shield, and an SD card so it would play songs of our choosing.

Parts used:

SparkFun Arduino Pro Mini 328
https://www.sparkfun.com/products/11113

AdaFruit Wave Shield
https://www.adafruit.com/product/94

AdaFruit Mintyboost
https://www.adafruit.com/product/14

SD card
